package com.example.simplesrssignaling;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.socket.WebSocketSession;

@Data
public class Participant {
    private Boolean publishing = false;
    private String display;
    @JsonIgnore
    private WebSocketSession session;
}
