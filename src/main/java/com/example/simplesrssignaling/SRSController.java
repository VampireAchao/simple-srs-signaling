package com.example.simplesrssignaling;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * /**
 * <pre>
 * <code>
 * http_hooks {
 *     enabled         on;
 *     on_publish      http://host.docker.internal:1989/api/v1/streams http://host.docker.internal:1989/api/v1/streams;
 *     on_unpublish    http://host.docker.internal:1989/api/v1/streams http://host.docker.internal:1989/api/v1/streams;
 *     on_play         http://host.docker.internal:1989/api/v1/sessions http://host.docker.internal:1989/api/v1/sessions;
 *     on_stop         http://host.docker.internal:1989/api/v1/sessions http://host.docker.internal:1989/api/v1/sessions;
 *     on_dvr          http://host.docker.internal:1989/api/v1/dvrs http://host.docker.internal:1989/api/v1/dvrs;
 *     on_hls          http://host.docker.internal:1989/api/v1/hls http://host.docker.internal:1989/api/v1/hls;
 *     on_hls_notify   http://host.docker.internal:1989/api/v1/hls/[server_id]/[app]/[stream]/[ts_url][param];
 * }
 * </code>
 * </pre>
 * use: docker run --rm --env CANDIDATE=$CANDIDATE -p 1935:1935 -p 8080:8080 -p 1985:1985 -p 8000:8000/udp -v /Users/achao/IdeaProjects/srs/trunk/conf/rtc.conf:/usr/local/srs/conf/rtc.conf registry.cn-hangzhou.aliyuncs.com/ossrs/srs:5 objs/srs -c /usr/local/srs/conf/rtc.conf
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1")
public class SRSController {

    @PostMapping("/streams")
    public ResponseEntity<Response> handlePublishAndUnpublish(@RequestBody HookData data) {
        // Handle the on_publish and on_unpublish actions
        return ResponseEntity.ok(new Response(0));
    }

    @PostMapping("/sessions")
    public ResponseEntity<Response> handlePlayAndStop(@RequestBody HookData data) {
        // Handle the on_play and on_stop actions
        return ResponseEntity.ok(new Response(0));
    }

    @PostMapping("/dvrs")
    public ResponseEntity<Response> handleDVR(@RequestBody HookData data) {
        // Handle the on_dvr action
        return ResponseEntity.ok(new Response(0));
    }

    @PostMapping("/hls")
    public ResponseEntity<Response> handleHLS(@RequestBody HookData data) {
        // Handle the on_hls action
        return ResponseEntity.ok(new Response(0));
    }

    @Data
    public static class HookData {
        private String action;
        private String client_id;
        private String ip;
        private String vhost;
        private String app;
        private String stream;
        private String param;
        // ... other fields

        // Getter and Setter methods
    }

    @Data
    public static class Response {
        private Integer code;
        private String msg;

        public Response(int code) {
            this.code = code;
            this.msg = "ok";
        }
    }
}
