package com.example.simplesrssignaling;

import lombok.Data;

@Data
public class ActionMessage {
    private String tid;
    private MessageContent msg;
}
