package com.example.simplesrssignaling;

import lombok.Data;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Data
public class Room {
    private String name;
    private List<Participant> participants = new ArrayList<>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public Room(String name) {
        this.name = name;
    }

    // Assuming you have a constructor, getters, setters, etc. for the Room class.
    public void add(Participant p) {
        lock.writeLock().lock();
        try {
            for (Participant r : participants) {
                if (r.getDisplay().equals(p.getDisplay())) {
                    remove(r);
                }
            }
            participants.add(p);
        } finally {
            lock.writeLock().unlock();
        }
    }

    // Assuming you have a constructor, getters, setters, etc. for the Room class.
    public Participant get(String display) {
        lock.readLock().lock();
        try {
            for (Participant r : participants) {
                if (r.getDisplay().equals(display)) {
                    return r;
                }
            }
            return null;
        } finally {
            lock.readLock().unlock();
        }
    }

    // Assuming you have a constructor, getters, setters, etc. for the Room class.

    public void remove(Participant p) {
        lock.writeLock().lock();
        try {
            participants.remove(p);
        } finally {
            lock.writeLock().unlock();
        }
    }

    // Assuming you have constructors, getters, setters, etc. for the Room class.
    public void notify(WebSocketSession session, Participant peer, String event, ActionMessage actionMessage) {
        for (Participant r : participants) {
            var res = new HashMap<>();
            res.put("action", "notify");
            res.put("event", event);
            res.put("room", name);
            res.put("self", r); // Assuming you have a toJson method in Participant
            res.put("peer", peer);
            res.put("participants", participants); // Convert list of participants to JSONArray
            res.put("data", actionMessage.getMsg().getData());
            res.put("param", actionMessage.getMsg().getCall());

            var message = new HashMap<>();
            message.put("msg", res);
            message.put("tid", actionMessage.getTid());

            // Sending the message to r
            // Assuming you have a mechanism in Participant class to send messages (e.g., an out channel in Go).
            if (r.getSession().isOpen()) {
                try {
                    r.getSession().sendMessage(new TextMessage(JacksonUtils.toJson(message)));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            // Logging or any other operation after sending the message
        }
    }

}
