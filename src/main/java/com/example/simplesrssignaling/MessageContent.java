package com.example.simplesrssignaling;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MessageContent {
    private String action;
    private String event;
    private String room;
    private String display;
    private String param;
    private String data;
    private String call;
    private List<Participant> participants = new ArrayList<>();
}