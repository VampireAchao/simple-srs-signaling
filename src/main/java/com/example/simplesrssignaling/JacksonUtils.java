package com.example.simplesrssignaling;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

/**
 * JacksonUtils
 *
 * @author VampireAchao<achao @ hutool.cn>
 * @since 2023/10/9
 */
@UtilityClass
public class JacksonUtils {

    @SneakyThrows
    public static String toJson(Object object) {
        return new ObjectMapper().writeValueAsString(object);
    }

}
