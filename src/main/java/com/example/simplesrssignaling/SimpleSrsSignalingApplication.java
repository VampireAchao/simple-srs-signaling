package com.example.simplesrssignaling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleSrsSignalingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleSrsSignalingApplication.class, args);
    }

}
